<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collapse extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Collapse_model', 'collapse_elem');
    }

    /** 
     * Carrega os elementos Collapse na view collapse.php
     */ 
    public function index(){
        $dados['titulo'] = 'How 2 Use';
        $data['collapse_elem'] = $this->collapse_elem->collapse_elem();
        $data['collapse_dup'] = $this->collapse_elem->collapse_dup();

        $this->load->view('common/header', $dados);
        $this->load->view('common/navbar');
        $this->load->view('collapse', $data);
        $this->load->view('common/footer');
    }

}
