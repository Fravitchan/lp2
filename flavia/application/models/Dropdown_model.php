<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/componentes/Dropdown.php';

class Dropdown_model extends CI_Model{


    /** 
     * Gera um elemento Dropdown
     * @return: getHTML()
     */ 
    public function drop_menu(){

        $class_button_drop = "btn btn-primary dropdown-toggle mr-4";
        $rotulo_button_drop = "Basic dropdown";
        $drop_link1 = "#";
        $drop_link2 = "#";
        $drop_link3 = "#";
        $drop_link4 = "#";
        $drop_acao1 = "Action";
        $drop_acao2 = "Another action";
        $drop_acao3 = "Something else here";
        $drop_acao4 = "Separated link";

        $drop_menu = new Dropdownlib($class_button_drop, $rotulo_button_drop, $drop_link1, 
        $drop_link2, $drop_link3, $drop_link4, $drop_acao1, $drop_acao2, $drop_acao3, $drop_acao4);

        return $drop_menu->getHTML();

    }

    /** 
     * Gera um elemento Filter Dropdown
     * @return: getHTML()
     */ 
    public function drop_filter_menu(){

        $drop_titulo = "Dropdown";
        $drop_placeholder = "Search..";
        $dropF_link1 = "#about";
        $dropF_link2 = "#base";
        $dropF_link3 = "#blog";
        $dropF_link4 = "#contact";
        $dropF_link5 = "#custom";
        $dropF_link6 = "#support";
        $dropF_link7 = "#tools";
        $dropF_acao1 = "About";
        $dropF_acao2 = "Base";
        $dropF_acao3 = "Blog";
        $dropF_acao4 = "Contact";
        $dropF_acao5 = "Custom";
        $dropF_acao6 = "Support";
        $dropF_acao7 = "Tools";

        $drop_filter_menu = new Filter_Dropdownlib($drop_titulo, $drop_placeholder, $dropF_link1, $dropF_link2, $dropF_link3 ,$dropF_link4,
        $dropF_link5, $dropF_link6, $dropF_link7, $dropF_acao1, $dropF_acao2, $dropF_acao3, $dropF_acao4, $dropF_acao5, 
        $dropF_acao6, $dropF_acao7);

        return $drop_filter_menu->getHTML();
 
    }

}