<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/componentes/FilterTable.php';

class FilterTable_model extends CI_Model{

    /** 
     * Gera um elemento filter table
     * @return: getHTML()
     */ 
    public function tabela_exemplo(){

        $titulo_tabela = "Registro de Alunos";
        $placeholder_frase = "Pesquisar nome...";
        $title = "Type in a name";
        $coluna1 = "Personagem";
        $coluna2 = "Casa de Hogwarts";
        $nome1 = "Harry Tiago Potter";
        $nome2 = "Alvo Percival Wulfric Brian Dumbledore";
        $nome3 = "Arthur Weasley";
        $nome4 = "Cho Chang";
        $nome5 = "Bellatrix Lestrange";
        $nome6 = "Draco Lúcio Malfoy";
        $nome7 = "Cedrico Diggory";
        $nome8 = "Dolores Jane Umbridge";
        $nome9 = "Sybill Patricia Trelawney";
        $nome10 = "Ernesto Macmillan";
        $nome11 = "Lúcio Malfoy";
        $nome12 = "Luna Lovegood";
        $nome13 = "Hermione Jean Granger";
        $nome14 = "Sirius Black";
        $nome15 = "Tom Marvolo Riddle";
        $nome16 = "Ronald Weasley";
        $casa1 = "Grifinória";
        $casa2 = "Grifinória";
        $casa3 = "Grifinória";
        $casa4 = "Corvinal";
        $casa5 = "Sonserina";
        $casa6 = "Sonserina";
        $casa7 = "Lufa-lufa";
        $casa8 = "Sonserina";
        $casa9 = "Corvinal";
        $casa10 = "Lufa-lufa";
        $casa11 = "Sonserina";
        $casa12 = "Corvinal";
        $casa13 = "Grifinória";
        $casa14 = "Grifinória";
        $casa15 = "Sonserina";
        $casa16 = "Grifinória";

        $tabela_exemplo = new FilterTablelib($titulo_tabela, $placeholder_frase, $title, $coluna1, $coluna2, $nome1, $nome2, $nome3,
        $nome4, $nome5, $nome6, $nome7, $nome8, $nome9, $nome10, $nome11, $nome12, $nome13, $nome14, $nome15, $nome16, 
        $casa1, $casa2, $casa3, $casa4, $casa5, $casa6, $casa7, $casa8, $casa9, $casa10, $casa11, $casa12, 
        $casa13, $casa14, $casa15, $casa16);

        return $tabela_exemplo->getHTML();

    }

}