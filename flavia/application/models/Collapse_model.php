<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/componentes/Collapse.php';

class Collapse_model extends CI_Model{

    /** 
     * Gera um elemento collapse
     * @return: getHTML()
     */ 
    public function collapse_elem(){

        $class_link = "btn btn-deep-purple accent-4";
        $link = "#collapseExample";
        $aria_controls_link = "collapseExample";
        $rotulo_link = "Link with href";
        $class_button = "btn btn-primary";
        $target_button = "#collapseExample";
        $aria_controls_button = "collapseExample";
        $rotulo_button = "Button with data-target";
        $class_collapse = "collapse";
        $id_collapse = "collapseExample";
        $id_collapseB = "teste";
        $conteudo = "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim
        keffiyeh helvetica,
        craft beer labore wes anderson cred nesciunt sapiente ea proident.";

        $collapse_elem = new Collapselib($class_link, $link, $aria_controls_link, $rotulo_link, $class_button, 
        $target_button, $aria_controls_button, $rotulo_button,
         $class_collapse, $id_collapse, $id_collapseB, $conteudo);
 

        return $collapse_elem->getHTML();
    
    }

    /** 
     * Gera um elemento collapse
     * @return: getHTML()
     */ 
    public function collapse_dup(){

        //Duplo
        $class_link = "btn btn-amber darken-4";
        $link = "#multiCollapseExample1";
        $aria_controls_link = "multiCollapseExample1";
        $rotulo_link = "Toggle first element";
        $class_button = "btn btn-primary";
        $target_button = ".multi-collapse";
        $aria_controls_button = "multiCollapseExample1 multiCollapseExample2";
        $rotulo_button = "Toggle both elements";
        $class_collapse = "collapse multi-collapse";
        $id_collapse = "multiCollapseExample1";
        $id_collapseB = "multiCollapseExample2";
        $conteudo = "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim
        keffiyeh helvetica,
        craft beer labore wes anderson cred nesciunt sapiente ea proident.";

        $collapse_dup = new Collapselib($class_link, $link, $aria_controls_link, $rotulo_link, $class_button, 
        $target_button, $aria_controls_button, $rotulo_button,
        $class_collapse, $id_collapse, $id_collapseB, $conteudo);

        return $collapse_dup->getHTML();

    }

}