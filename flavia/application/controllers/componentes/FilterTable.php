<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FilterTable extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('FilterTable_model', 'tabela_exemplo');
    }

    /** 
     * Carrega o elemento Filter Table na view filter_table.php
     */
    public function index(){
        $dados['titulo'] = 'How 2 Use';
        $data['tabela_exemplo'] = $this->tabela_exemplo->tabela_exemplo();

        $this->load->view('common/header', $dados);
        $this->load->view('common/navbar');
        $this->load->view('filter_table', $data);
        $this->load->view('common/footer');
    }

}