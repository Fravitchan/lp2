<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dropdown extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Dropdown_model', 'drop_menu');
        $this->load->model('Dropdown_model', 'drop_filter_menu');
    }

    /** 
     * Carrega os elementos Dropdown na view dropdown.php
     */
    public function index(){
        $dados['titulo'] = 'How 2 Use';
        $data['drop_menu'] = $this->drop_menu->drop_menu();
        $data['drop_filter_menu'] = $this->drop_filter_menu->drop_filter_menu();

        $this->load->view('common/header', $dados);
        $this->load->view('common/navbar');
        $this->load->view('dropdown', $data);
        $this->load->view('common/footer');
    }

}