<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

     function __construct(){
         parent::__construct();
        //  $this->load->helper('url');
     }

	public function index(){
        $dados['titulo'] = 'How 2 Use';

        $this->load->view('common/header', $dados);
        $this->load->view('common/navbar');
        $this->load->view('homepage');
        $this->load->view('common/footer');

	}
}