<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include_once APPPATH . '/libraries/componentes/Collapse.php';
include_once APPPATH . '/libraries/componentes/Dropdown.php';
include_once APPPATH . '/libraries/componentes/FilterTable.php';


class Test_flavia extends MyToast{

    function __construct(){
        parent:: __construct('Test_flavia');
    }

    // TESTES

    /**
     * Collapse teste 1
     * Esta função testa se os valores passados na variável $conteudo_obj são iguais.
     */
    function test_conteudo_equal(){

        $class_link = "btn btn-amber darken-4";
        $link = "#multiCollapseExample1";
        $aria_controls_link = "multiCollapseExample1";
        $rotulo_link = "Toggle first element";
        $class_button = "btn btn-primary";
        $target_button = ".multi-collapse";
        $aria_controls_button = "multiCollapseExample1 multiCollapseExample2";
        $rotulo_button = "Toggle both elements";
        $class_collapse = "collapse multi-collapse";
        $id_collapse = "multiCollapseExample1";
        $id_collapseB = "multiCollapseExample2";
        $conteudo = "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim
        keffiyeh helvetica,
        craft beer labore wes anderson cred nesciunt sapiente ea proident.";

        $conteudo_obj = new CollapseLib($class_link, $link, $aria_controls_link, $rotulo_link, $class_button, 
        $target_button, $aria_controls_button, $rotulo_button,
        $class_collapse, $id_collapse, $id_collapseB, $conteudo);

        $conteudo_obj->setConteudo('Flavia');
        $result = $conteudo_obj->getConteudo();

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_equals('Flavia',$result, "Erro: esperado (Flavia), recebido ($result)");

    }

    /**
     * Collapse teste 2
     * Esta função exibe verdadeiro se o valor informado em $rotulo_obj 
     * for diferente de 5, caso contrário, exibe a mensagem de erro.
     */
    function test_rotulo_button_true(){

        $class_link = "btn btn-amber darken-4";
        $link = "#multiCollapseExample1";
        $aria_controls_link = "multiCollapseExample1";
        $rotulo_link = "Toggle first element";
        $class_button = "btn btn-primary";
        $target_button = ".multi-collapse";
        $aria_controls_button = "multiCollapseExample1 multiCollapseExample2";
        $rotulo_button = "Toggle both elements";
        $class_collapse = "collapse multi-collapse";
        $id_collapse = "multiCollapseExample1";
        $id_collapseB = "multiCollapseExample2";
        $conteudo = "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim
        keffiyeh helvetica,
        craft beer labore wes anderson cred nesciunt sapiente ea proident.";

        $rotulo_obj = new CollapseLib($class_link, $link, $aria_controls_link, $rotulo_link, $class_button, 
        $target_button, $aria_controls_button, $rotulo_button,
        $class_collapse, $id_collapse, $id_collapseB, $conteudo);

        $rotulo_obj->setRotuloButton('4');
        $var = $rotulo_obj->getRotuloButton();
        $result = $var != 5;

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_true($result, 'Erro: não pode ser (5)');
    }

    /**
     * Collapse teste 3
     * Esta função testa se o valor sorteado na variável $link_obj 
     * é maior do que 10. Em caso positivo, retorna a mensagem de erro. 
     * Caso contrário, o teste apresentará sucesso.
     */
    function test_link_false(){

        $class_link = "btn btn-amber darken-4";
        $link = "#multiCollapseExample1";
        $aria_controls_link = "multiCollapseExample1";
        $rotulo_link = "Toggle first element";
        $class_button = "btn btn-primary";
        $target_button = ".multi-collapse";
        $aria_controls_button = "multiCollapseExample1 multiCollapseExample2";
        $rotulo_button = "Toggle both elements";
        $class_collapse = "collapse multi-collapse";
        $id_collapse = "multiCollapseExample1";
        $id_collapseB = "multiCollapseExample2";
        $conteudo = "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim
        keffiyeh helvetica,
        craft beer labore wes anderson cred nesciunt sapiente ea proident.";

        $link_obj = new CollapseLib($class_link, $link, $aria_controls_link, $rotulo_link, $class_button, 
        $target_button, $aria_controls_button, $rotulo_button,
        $class_collapse, $id_collapse, $id_collapseB, $conteudo);

        $link_obj = rand(0, 20);
        $num = $link_obj;
        $result = $num > 10;

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_false($result, "Erro: não pode ser maior que (10). Recebido: ($link_obj).");

    }


    /**
     * Dropdown teste 1
     * Esta função testa se a variável $drop_obj está vazia.
     * Em caso positivo, o teste terá sucesso, caso contrário,
     * uma mensagem de erro será exibida.
     */
    function test_drop_link1_empty(){

        $class_button_drop = "btn btn-primary dropdown-toggle mr-4";
        $rotulo_button_drop = "Basic dropdown";
        $drop_link1 = "#";
        $drop_link2 = "#";
        $drop_link3 = "#";
        $drop_link4 = "#";
        $drop_acao1 = "Action";
        $drop_acao2 = "Another action";
        $drop_acao3 = "Something else here";
        $drop_acao4 = "Separated link";

        $drop_obj = new Dropdownlib($class_button_drop, $rotulo_button_drop, $drop_link1, 
        $drop_link2, $drop_link3, $drop_link4, $drop_acao1, $drop_acao2, $drop_acao3, $drop_acao4);

        $drop_obj->setDrop1('');
        $result = $drop_obj->getDrop1();

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_empty($result, 'Não pode ser preenchido.');
    }

    /**
     * Dropdown teste 2
     * Esta função testa se a variável $drop_obj está preenchida.
     * Em caso positivo, o teste terá sucesso, caso contrário,
     * uma mensagem de erro será exibida.
     */
    function test_drop_link2_not_empty(){

        $class_button_drop = "btn btn-primary dropdown-toggle mr-4";
        $rotulo_button_drop = "Basic dropdown";
        $drop_link1 = "#";
        $drop_link2 = "#";
        $drop_link3 = "#";
        $drop_link4 = "#";
        $drop_acao1 = "Action";
        $drop_acao2 = "Another action";
        $drop_acao3 = "Something else here";
        $drop_acao4 = "Separated link";

        $drop_obj = new Dropdownlib($class_button_drop, $rotulo_button_drop, $drop_link1, 
        $drop_link2, $drop_link3, $drop_link4, $drop_acao1, $drop_acao2, $drop_acao3, $drop_acao4);

        $drop_obj->setDrop2('harrypotter');
        $result = $drop_obj->getDrop2();

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_not_empty($result, 'Não pode ser vazio.');
    }

    /**
     * Dropdown teste 3
     * Esta função testa se o valor informado é igual ao valor contido na 
     * variável $drop_obj.
     * Em caso positivo, o teste terá sucesso, caso contrário,
     * uma mensagem de erro será exibida.
     */
    function test_drop_link3_equal(){

        $class_button_drop = "btn btn-primary dropdown-toggle mr-4";
        $rotulo_button_drop = "Basic dropdown";
        $drop_link1 = "#";
        $drop_link2 = "#";
        $drop_link3 = "#";
        $drop_link4 = "#";
        $drop_acao1 = "Action";
        $drop_acao2 = "Another action";
        $drop_acao3 = "Something else here";
        $drop_acao4 = "Separated link";

        $drop_obj = new Dropdownlib($class_button_drop, $rotulo_button_drop, $drop_link1, 
        $drop_link2, $drop_link3, $drop_link4, $drop_acao1, $drop_acao2, $drop_acao3, $drop_acao4);

        $drop_obj->setDrop3('chocolate');
        $result = $drop_obj->getDrop3();

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_equals('chocolate', $result, 'Não pode ser diferente.');
    }
    
    /**
     * Filter Table teste 1
     * Esta função testa se o valor informado é diferente do valor contido na 
     * variável $tabela_obj.
     * Em caso positivo, o teste terá sucesso, caso contrário,
     * uma mensagem de erro será exibida.
     */
    function test_titulo_tabela_not_equal(){

        $titulo_tabela = "Registro de Alunos";
        $placeholder_frase = "Pesquisar nome...";
        $title = "Type in a name";
        $coluna1 = "Personagem";
        $coluna2 = "Casa de Hogwarts";
        $nome1 = "Harry Tiago Potter";
        $nome2 = "Alvo Percival Wulfric Brian Dumbledore";
        $nome3 = "Arthur Weasley";
        $nome4 = "Cho Chang";
        $nome5 = "Bellatrix Lestrange";
        $nome6 = "Draco Lúcio Malfoy";
        $nome7 = "Cedrico Diggory";
        $nome8 = "Dolores Jane Umbridge";
        $nome9 = "Sybill Patricia Trelawney";
        $nome10 = "Ernesto Macmillan";
        $nome11 = "Lúcio Malfoy";
        $nome12 = "Luna Lovegood";
        $nome13 = "Hermione Jean Granger";
        $nome14 = "Sirius Black";
        $nome15 = "Tom Marvolo Riddle";
        $nome16 = "Ronald Weasley";
        $casa1 = "Grifinória";
        $casa2 = "Grifinória";
        $casa3 = "Grifinória";
        $casa4 = "Corvinal";
        $casa5 = "Sonserina";
        $casa6 = "Sonserina";
        $casa7 = "Lufa-lufa";
        $casa8 = "Sonserina";
        $casa9 = "Corvinal";
        $casa10 = "Lufa-lufa";
        $casa11 = "Sonserina";
        $casa12 = "Corvinal";
        $casa13 = "Grifinória";
        $casa14 = "Grifinória";
        $casa15 = "Sonserina";
        $casa16 = "Grifinória";

        $tabela_obj = new FilterTablelib($titulo_tabela, $placeholder_frase, $title, $coluna1, $coluna2, $nome1, $nome2, $nome3,
        $nome4, $nome5, $nome6, $nome7, $nome8, $nome9, $nome10, $nome11, $nome12, $nome13, $nome14, $nome15, $nome16, 
        $casa1, $casa2, $casa3, $casa4, $casa5, $casa6, $casa7, $casa8, $casa9, $casa10, $casa11, $casa12, 
        $casa13, $casa14, $casa15, $casa16);

        $tabela_obj->set_titulo_tabela('alunos');
        $result = $tabela_obj->get_titulo_tabela();

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_not_equals('professores', $result, 'Não pode ser igual.');
    }

    /**
     * Filter Table teste 2
     * Esta função testa se o valor informado é igual ao valor contido na 
     * variável $tabela_obj.
     * Em caso positivo, o teste terá sucesso, caso contrário,
     * uma mensagem de erro será exibida.
     */
    function test_placeholder_frase_equal(){

        $titulo_tabela = "Registro de Alunos";
        $placeholder_frase = "Pesquisar nome...";
        $title = "Type in a name";
        $coluna1 = "Personagem";
        $coluna2 = "Casa de Hogwarts";
        $nome1 = "Harry Tiago Potter";
        $nome2 = "Alvo Percival Wulfric Brian Dumbledore";
        $nome3 = "Arthur Weasley";
        $nome4 = "Cho Chang";
        $nome5 = "Bellatrix Lestrange";
        $nome6 = "Draco Lúcio Malfoy";
        $nome7 = "Cedrico Diggory";
        $nome8 = "Dolores Jane Umbridge";
        $nome9 = "Sybill Patricia Trelawney";
        $nome10 = "Ernesto Macmillan";
        $nome11 = "Lúcio Malfoy";
        $nome12 = "Luna Lovegood";
        $nome13 = "Hermione Jean Granger";
        $nome14 = "Sirius Black";
        $nome15 = "Tom Marvolo Riddle";
        $nome16 = "Ronald Weasley";
        $casa1 = "Grifinória";
        $casa2 = "Grifinória";
        $casa3 = "Grifinória";
        $casa4 = "Corvinal";
        $casa5 = "Sonserina";
        $casa6 = "Sonserina";
        $casa7 = "Lufa-lufa";
        $casa8 = "Sonserina";
        $casa9 = "Corvinal";
        $casa10 = "Lufa-lufa";
        $casa11 = "Sonserina";
        $casa12 = "Corvinal";
        $casa13 = "Grifinória";
        $casa14 = "Grifinória";
        $casa15 = "Sonserina";
        $casa16 = "Grifinória";

        $tabela_obj = new FilterTablelib($titulo_tabela, $placeholder_frase, $title, $coluna1, $coluna2, $nome1, $nome2, $nome3,
        $nome4, $nome5, $nome6, $nome7, $nome8, $nome9, $nome10, $nome11, $nome12, $nome13, $nome14, $nome15, $nome16, 
        $casa1, $casa2, $casa3, $casa4, $casa5, $casa6, $casa7, $casa8, $casa9, $casa10, $casa11, $casa12, 
        $casa13, $casa14, $casa15, $casa16);

        $tabela_obj->set_placeholder_frase('pesquisar');
        $result = $tabela_obj->get_placeholder_frase();

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_equals_strict('pesquisar', $result, 'Não pode ser diferente.');
    }

    /**
     * Filter Table teste 3
     * Esta função testa se o valor informado é diferente do valor contido na 
     * variável $tabela_obj.
     * Em caso positivo, o teste terá sucesso, caso contrário,
     * uma mensagem de erro será exibida.
     */
    function test_nome1_not_equal(){

        $titulo_tabela = "Registro de Alunos";
        $placeholder_frase = "Pesquisar nome...";
        $title = "Type in a name";
        $coluna1 = "Personagem";
        $coluna2 = "Casa de Hogwarts";
        $nome1 = "Harry Tiago Potter";
        $nome2 = "Alvo Percival Wulfric Brian Dumbledore";
        $nome3 = "Arthur Weasley";
        $nome4 = "Cho Chang";
        $nome5 = "Bellatrix Lestrange";
        $nome6 = "Draco Lúcio Malfoy";
        $nome7 = "Cedrico Diggory";
        $nome8 = "Dolores Jane Umbridge";
        $nome9 = "Sybill Patricia Trelawney";
        $nome10 = "Ernesto Macmillan";
        $nome11 = "Lúcio Malfoy";
        $nome12 = "Luna Lovegood";
        $nome13 = "Hermione Jean Granger";
        $nome14 = "Sirius Black";
        $nome15 = "Tom Marvolo Riddle";
        $nome16 = "Ronald Weasley";
        $casa1 = "Grifinória";
        $casa2 = "Grifinória";
        $casa3 = "Grifinória";
        $casa4 = "Corvinal";
        $casa5 = "Sonserina";
        $casa6 = "Sonserina";
        $casa7 = "Lufa-lufa";
        $casa8 = "Sonserina";
        $casa9 = "Corvinal";
        $casa10 = "Lufa-lufa";
        $casa11 = "Sonserina";
        $casa12 = "Corvinal";
        $casa13 = "Grifinória";
        $casa14 = "Grifinória";
        $casa15 = "Sonserina";
        $casa16 = "Grifinória";

        $tabela_obj = new FilterTablelib($titulo_tabela, $placeholder_frase, $title, $coluna1, $coluna2, $nome1, $nome2, $nome3,
        $nome4, $nome5, $nome6, $nome7, $nome8, $nome9, $nome10, $nome11, $nome12, $nome13, $nome14, $nome15, $nome16, 
        $casa1, $casa2, $casa3, $casa4, $casa5, $casa6, $casa7, $casa8, $casa9, $casa10, $casa11, $casa12, 
        $casa13, $casa14, $casa15, $casa16);

        $tabela_obj->set_nome1('Hermione');
        $result = $tabela_obj->get_nome1();

        //Exibe o resultado do teste e mensagem de erro em caso de falha
        $this->_assert_not_equals_strict('Hulk', $result, 'Não pode ser igual.');
    }

}