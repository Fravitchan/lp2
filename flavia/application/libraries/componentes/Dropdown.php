<?php

include_once 'Component.php';

class DropdownLib extends Component{

    //Atributos

    /** 
     * Declaracao de atributos do Dropdown
     */ 
    private $class_button_drop;
    private $rotulo_button_drop;
    private $drop_link1;
    private $drop_link2;
    private $drop_link3;
    private $drop_link4;
    private $drop_acao1;
    private $drop_acao2;
    private $drop_acao3;
    private $drop_acao4;


    //Construtor
    function __construct($class_button_drop, $rotulo_button_drop, $drop_link1, 
    $drop_link2, $drop_link3, $drop_link4, $drop_acao1, $drop_acao2, $drop_acao3, $drop_acao4){
        $this->class_button_drop = $class_button_drop;
        $this->rotulo_button_drop = $rotulo_button_drop;
        $this->drop_link1 = $drop_link1;
        $this->drop_link2 = $drop_link2;
        $this->drop_link3 = $drop_link3;
        $this->drop_link4 = $drop_link4;
        $this->drop_acao1 = $drop_acao1;
        $this->drop_acao2 = $drop_acao2;
        $this->drop_acao3 = $drop_acao3;
        $this->drop_acao4 = $drop_acao4;
    }

    //Métodos

    /** 
     * Gera o código HTML do Dropdown
     * @return string: código html
     */ 
    public function getHTML(){
        $html = $this->dropdown_button();
        $html.= $this->dropdown_menu();

        return $html;
    }

    /** 
     * Gera o código do botão do Dropdown
     * @return string: código html
     */ 
    private function dropdown_button(){
        $html = '<button class="'.$this->class_button_drop.'" type="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">'.$this->rotulo_button_drop.'</button>';

        return $html;
    }

    /** 
     * Gera o código do Menu Dropdown
     * @return string: código html
     */ 
    private function dropdown_menu(){
        $html = '<div class="dropdown-menu">
        <a class="dropdown-item" href="'.$this->drop_link1.'">'.$this->drop_acao1.'</a>
        <a class="dropdown-item" href="'.$this->drop_link2.'">'.$this->drop_acao2.'</a>
        <a class="dropdown-item" href="'.$this->drop_link3.'">'.$this->drop_acao3.'</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="'.$this->drop_link4.'">'.$this->drop_acao4.'</a>
      </div>';

      return $html;
    }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function getDrop1(){
        return $this->drop_link1;
    }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function getDrop2(){
        return $this->drop_link2;
    }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function getDrop3(){
        return $this->drop_link3;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function setDrop1($drop_link1){
        $this->drop_link1 = $drop_link1;
        return $this;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function setDrop2($drop_link2){
        $this->drop_link2 = $drop_link2;
        return $this;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function setDrop3($drop_link3){
        $this->drop_link3 = $drop_link3;
        return $this;
    }


}

class Filter_DropdownLib extends Component{

    //Atributos

    /** 
     * Declaracao de atributos do  Filter Dropdown
     */ 
    private $drop_titulo;
    private $drop_placeholder;
    private $dropF_link1;
    private $dropF_link2;
    private $dropF_link3;
    private $dropF_link4;
    private $dropF_link5;
    private $dropF_link6;
    private $dropF_link7;
    private $dropF_acao1;
    private $dropF_acao2;
    private $dropF_acao3;
    private $dropF_acao4;
    private $dropF_acao5;
    private $dropF_acao6;
    private $dropF_acao7;
     

    //Construtor
    function __construct($drop_titulo, $drop_placeholder, $dropF_link1, $dropF_link2, $dropF_link3 ,$dropF_link4,
    $dropF_link5, $dropF_link6, $dropF_link7, $dropF_acao1, $dropF_acao2, $dropF_acao3, $dropF_acao4, $dropF_acao5, 
    $dropF_acao6, $dropF_acao7){
        $this->drop_titulo  =  $drop_titulo;
        $this->drop_placeholder =  $drop_placeholder;
        $this->dropF_link1 = $dropF_link1;
        $this->dropF_link2 = $dropF_link2;
        $this->dropF_link3 = $dropF_link3;
        $this->dropF_link4 = $dropF_link4;
        $this->dropF_link5 = $dropF_link5;
        $this->dropF_link6 = $dropF_link6;
        $this->dropF_link7 = $dropF_link7;
        $this->dropF_acao1 = $dropF_acao1;
        $this->dropF_acao2 = $dropF_acao2;
        $this->dropF_acao3 = $dropF_acao3;
        $this->dropF_acao4 = $dropF_acao4;
        $this->dropF_acao5 = $dropF_acao5;
        $this->dropF_acao6 = $dropF_acao6;
        $this->dropF_acao7 = $dropF_acao7;
    }

    //Métodos

    /** 
     * Gera o código do Filter Dropdown
     * @return string: código html
     */ 
    public function getHTML(){

        $html = $this->filter_dropdown_with_menu();

        return $html;
    }


    /** 
     * Gera o código do elemento Menu Dropdown
     * @return string: código html
     */ 
    private function filter_dropdown_with_menu(){
        $html = '<div class="dropdown">
        <button onclick="myFunction2()" class="dropbtn">'.$this->drop_titulo.'</button>
        <div id="myDropdown" class="dropdown-content">
          <input type="text" placeholder="'.$this->drop_placeholder.'" id="myInput" onkeyup="filterFunction()">
          <a href="'.$this->dropF_link1.'">'.$this->dropF_acao1.'</a>
          <a href="'.$this->dropF_link2.'">'.$this->dropF_acao2.'</a>
          <a href="'.$this->dropF_link3.'">'.$this->dropF_acao3.'</a>
          <a href="'.$this->dropF_link4.'">'.$this->dropF_acao4.'</a>
          <a href="'.$this->dropF_link5.'">'.$this->dropF_acao5.'</a>
          <a href="'.$this->dropF_link6.'">'.$this->dropF_acao6.'</a>
          <a href="'.$this->dropF_link7.'">'.$this->dropF_acao7.'</a>
        </div>
      </div>';

      return $html;
    }

    

}