<?php

include_once 'Component.php';

class CollapseLib extends Component{

    //Atributos

    /** 
     * Declaracao de atributos do Collapse
     */ 
    private $class_link = "btn btn-amber darken-4";
    private $link = "#multiCollapseExample1";
    private $aria_controls_link = "multiCollapseExample1";
    private $rotulo_link = "Toggle first element";
    private $class_button = "btn btn-primary";
    private $target_button = ".multi-collapse";
    private $aria_controls_button = "multiCollapseExample1 multiCollapseExample2";
    private $rotulo_button = "Toggle both elements";
    private $class_collapse = "collapse multi-collapse";
    private $id_collapse = "multiCollapseExample1";
    private $id_collapseB = "multiCollapseExample2";
    private $conteudo = "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson 
    ad squid. Nihil anim keffiyeh helvetica,
    craft beer labore wes anderson cred nesciunt sapiente ea proident.";

    //Construtor
    function __construct($class_link, $link, $aria_controls_link, $rotulo_link, $class_button, 
    $target_button, $aria_controls_button, $rotulo_button,
    $class_collapse, $id_collapse, $id_collapseB, $conteudo){
        $this->class_link = $class_link;
        $this->link = $link;
        $this->aria_controls_link = $aria_controls_link;
        $this->rotulo_link = $rotulo_link;
        $this->class_button = $class_button;
        $this->target_button = $target_button;
        $this->aria_controls_button = $aria_controls_button;
        $this->rotulo_button = $rotulo_button;
        $this->class_collapse = $class_collapse;
        $this->id_collapse = $id_collapse;
        $this->id_collapseB = $id_collapseB;
        $this->conteudo = $conteudo;
    }

    //Métodos

    /** 
     * Gera o código HTML do Collapse
     * @return string: código html
     */ 
    public function getHTML(){
        $html = $this->collapse_link();
        $html.= $this->collapse_button();
        $html.= $this->collapse_elemento();
        $html.= $this->collapse_duplo();

        return $html;
    }

    /** 
     * Gera o código do botão de link do Collapse
     * @return string: código html
     */ 
    private function collapse_link(){
        $html = '<div><a class="'.$this->class_link.'" data-toggle="collapse" href="'.$this->link.'" aria-expanded="false" 
        aria-controls="'.$this->aria_controls_link.'">'.$this->rotulo_link.'</a>';
        return $html;
    }

    /** 
     * Gera o código do botão do collapse
     * @return string: código html
     */ 
    private function collapse_button(){
        $html = '<button class="'.$this->class_button.'" type="button" data-toggle="collapse" data-target="'.$this->target_button.'"
            aria-expanded="false" aria-controls="'.$this->aria_controls_button.'">'.$this->rotulo_button.'</button>
        </div>';
        return $html;
    }

    /** 
     * Gera o código do elemento do collapse
     * @return string: código html
     */ 
    private function collapse_elemento(){
        $html = '<div class="row">
                    <div class="col">
                        <div class="'.$this->class_collapse.'" id="'.$this->id_collapse.'">
                            <div class="card card-body">'.$this->conteudo.'</div>
                        </div>
                    </div>';
        return $html;
    }

    /** 
     * Gera o código de collapse múltiplo
     * @return string: código html
     */ 
    private function collapse_duplo(){
        $html = '<div class="col">
                    <div class="'.$this->class_collapse.'" id="'.$this->id_collapseB.'">
                        <div class="card card-body">'.$this->conteudo.'</div>
                    </div>
                </div>
            </div>';
        return $html;
    }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function getConteudo(){
        return $this->conteudo;
    }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function getRotuloButton(){
        return $this->rotulo_button;
    }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function getLink(){
        return $this->link;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function setConteudo($conteudo){
        $this->conteudo = $conteudo;
        return $this;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function setRotuloButton($rotulo_button){
        $this->rotulo_button = $rotulo_button;
        return $this;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function setLink($link){
        $this->link = $link;
        return $this;
    }
}
