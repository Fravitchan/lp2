<?php

include_once 'Component.php';

class FilterTableLib extends Component{

    //Atributos

    /** 
     * Declaracao de atributos da Filter Table
     */ 
    private $titulo_tabela;
    private $placeholder_frase;
    private $title;
    private $coluna1;
    private $coluna2;
    private $nome1;
    private $nome2;
    private $nome3;
    private $nome4;
    private $nome5;
    private $nome6;
    private $nome7;
    private $nome8;
    private $nome9;
    private $nome10;
    private $nome11;
    private $nome12;
    private $nome13;
    private $nome14;
    private $nome15;
    private $nome16;
    private $casa1;
    private $casa2;
    private $casa3;
    private $casa4;
    private $casa5;
    private $casa6;
    private $casa7;
    private $casa8;
    private $casa9;
    private $casa10;
    private $casa11;
    private $casa12;
    private $casa13;
    private $casa14;
    private $casa15;
    private $casa16;


    //Construtor
    function __construct($titulo_tabela, $placeholder_frase, $title, $coluna1, $coluna2, $nome1, $nome2, $nome3,
    $nome4, $nome5, $nome6, $nome7, $nome8, $nome9, $nome10, $nome11, $nome12, $nome13, $nome14, $nome15, $nome16, 
    $casa1, $casa2, $casa3, $casa4, $casa5, $casa6, $casa7, $casa8, $casa9, $casa10, $casa11, $casa12, 
    $casa13, $casa14, $casa15, $casa16){
        $this->titulo_tabela = $titulo_tabela;
        $this->placeholder_frase = $placeholder_frase;
        $this->title = $title;
        $this->coluna1 = $coluna1;
        $this->coluna2 = $coluna2;
        $this->nome1 = $nome1;
        $this->nome2 = $nome2;
        $this->nome3 = $nome3;
        $this->nome4 = $nome4;
        $this->nome5 = $nome5;
        $this->nome6 = $nome6;
        $this->nome7 = $nome7;
        $this->nome8 = $nome8;
        $this->nome9 = $nome9;
        $this->nome10 = $nome10;
        $this->nome11 = $nome11;
        $this->nome12 = $nome12;
        $this->nome13 = $nome13;
        $this->nome14 = $nome14;
        $this->nome15 = $nome15;
        $this->nome16 = $nome16;
        $this->casa1 = $casa1;
        $this->casa2 = $casa2;
        $this->casa3 = $casa3;
        $this->casa4 = $casa4;
        $this->casa5 = $casa5;
        $this->casa6 = $casa6;
        $this->casa7 = $casa7;
        $this->casa8 = $casa8;
        $this->casa9 = $casa9;
        $this->casa10 = $casa10;
        $this->casa11 = $casa11;
        $this->casa12 = $casa12;
        $this->casa13 = $casa13;
        $this->casa14 = $casa14;
        $this->casa15 = $casa15;
        $this->casa16 = $casa16;
    }

    //Métodos

    /** 
     * Gera o código HTML da Filter Table
     * @return string: código html
     */ 
    public function getHTML(){
        $html = $this->tabela_filtro();
         
        return $html;
    }

    /** 
     * Gera o código do elemento Filter Table
     * @return string: código html
     */ 
     private function tabela_filtro(){
         $html = '<div class="minha_tabela">
                <h4 style="width:70%" align="center">'.$this->titulo_tabela.'</h4>

                <center>
                    <input style="width:70%" type="text" id="myInput" onkeyup="myFunction()" placeholder="'.$this->placeholder_frase.'" title="'.$this->title.'">
                </center>

                <table id="myTable" style="width:70%" align="center">
                <tr class="header">
                    <th class="text-white" style="width:60%;">'.$this->coluna1.'</th>
                    <th class="text-white" style="width:40%;">'.$this->coluna2.'</th>
                </tr>
                <tr>
                    <td>'.$this->nome1.'</td>
                    <td>'.$this->casa1.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome2.'</td>
                    <td>'.$this->casa2.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome3.'</td>
                    <td>'.$this->casa3.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome4.'</td>
                    <td>'.$this->casa4.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome5.'</td>
                    <td>'.$this->casa5.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome6.'</td>
                    <td>'.$this->casa6.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome7.'</td>
                    <td>'.$this->casa7.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome8.'</td>
                    <td>'.$this->casa8.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome9.'</td>
                    <td>'.$this->casa9.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome10.'</td>
                    <td>'.$this->casa10.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome11.'</td>
                    <td>'.$this->casa11.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome12.'</td>
                    <td>'.$this->casa12.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome13.'</td>
                    <td>'.$this->casa13.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome14.'</td>
                    <td>'.$this->casa14.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome15.'</td>
                    <td>'.$this->casa15.'</td>
                </tr>
                <tr>
                    <td>'.$this->nome16.'</td>
                    <td>'.$this->casa16.'</td>
                </tr>
                </table>
         </div>';

         return $html;
     }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function get_titulo_tabela(){
        return $this->titulo_tabela;
    }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function get_placeholder_frase(){
        return $this->placeholder_frase;
    }

    /**
     * Esta função pega o valor contido na variável.
     * @return string: variável
     */
    public function get_nome1(){
        return $this->nome1;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function set_titulo_tabela($titulo_tabela){
        $this->titulo_tabela = $titulo_tabela;
        return $this;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function set_placeholder_frase($placeholder_frase){
        $this->placeholder_frase = $placeholder_frase;
        return $this;
    }

    /**
     * Esta função permite setar o valor contido na variável.
     * @return string: variável
     */
    public function set_nome1($nome1){
        $this->nome1 = $nome1;
        return $this;
    }

}

