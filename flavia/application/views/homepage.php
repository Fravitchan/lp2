<br/>
<!--Main layout-->
<main class="mt-5 pt-5">
    <div class="container">

      <!--Section: Jumbotron-->
      <section class="card wow fadeIn" style="background-image: url(assets/img_site/gif10.gif);">

        <!-- Content -->
        <div class="card-body text-white text-center py-5 px-5 my-5">

          <h1 class="mb-4">
            <strong>Aprenda a utilizar componentes em seu site</strong>
          </h1>
          <p>
            <strong>Guia e documentação de apoio</strong>
          </p>
          <p class="mb-4">
            <strong>Instruções de utilização dos componentes do W3schools e Mdbootstrap.</strong>
          </p>
          <a target="_self" href="#How2Use" class="btn btn-outline-deep-purple accent-4 btn-lg">
            CONFERIR
            <i class="fas fa-star ml-2"></i>
          </a>

        </div>
        <!-- Content -->
      </section>
      <!--Section: Jumbotron-->

      <hr class="my-5">

      <!--Section: Magazine v.1-->
      <section class="wow fadeIn">

        <!--Section heading-->
        <h2 class="h1 text-center my-5 font-weight-bold" id="How2Use">How 2 Use</h2>

        <!-- Collapse buttons -->
        <div>
          <button class="btn btn-deep-purple" type="button" data-toggle="collapse" data-target=".multi-collapse"
            aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Mais Informações</button>
        </div>
        <!--/ Collapse buttons -->

        <!-- Collapsible content -->
        <div class="row">
          <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
              <div class="card card-body">
                Isto é um collapse! Legal, não é mesmo?
                Você também pode utilizar este e outros componentes em seu site!
              </div>
            </div>
          </div>
          <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
              <div class="card card-body">
                 Você pode aprender mais sobre collapse, dropdown e filter table com toda explicação
                 disponível aqui na How2Use. Não deixe de conferir!
              </div>
            </div>
          </div>
        </div>
        <!--/ Collapsible content -->

        <!--Grid row-->
        <div class="row text-left">

          <!--Grid column-->
          <div class="col-lg-6 col-md-12">

            <!--Image-->
            <div class="view overlay rounded z-depth-1-half mb-3">
              <img src="assets/img_site/img1.jpg" class="img-fluid" alt="Sample post image">
              <a>
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            

            <!--Excerpt-->
            <h3>
              <a>
                <strong>Componentes</strong>
              </a>
            </h3>

            <!--/Featured news-->

            <hr>

            <!--Small news-->
            <div class="row">
              <div class="col-md-3">

                <!--Image-->
                <div class="view overlay rounded z-depth-1">
                  <img src="assets/img_site/img3.jpg" class="img-fluid" alt="Minor sample post image">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
              </div>

              <!--Excerpt-->
              <div class="col-md-9">
                <p class="dark-grey-text">
                  <strong>Filter Table</strong>
                </p>
                <a href="<?php echo base_url(); ?>componentes/filtertable">Ver aplicação
                  <i class="fas fa-angle-right float-right"></i>
                </a>
              </div>

            </div>
            <!--/Small news-->

            <hr>

            <!--Small news-->
            <div class="row">
              <div class="col-md-3">

                <!--Image-->
                <div class="view overlay rounded z-depth-1">
                  <img src="assets/img_site/img4.jpg" class="img-fluid" alt="Minor sample post image">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
              </div>

              <!--Excerpt-->
              <div class="col-md-9">
                <p class="dark-grey-text">
                  <strong>Collapse</strong>
                </p>
                <a href="<?php echo base_url(); ?>componentes/collapse">Ver aplicação
                  <i class="fas fa-angle-right float-right"></i>
                </a>
              </div>

            </div>
            <!--/Small news-->

            <hr>

            <!--Small news-->
            <div class="row">
              <div class="col-md-3">

                <!--Image-->
                <div class="view overlay rounded z-depth-1">
                  <img src="assets/img_site/img2.jpg" class="img-fluid" alt="Minor sample post image">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
              </div>

              <!--Excerpt-->
              <div class="col-md-9">
                <p class="dark-grey-text">
                  <strong>Dropdown</strong>
                </p>
                <a href="<?php echo base_url(); ?>componentes/dropdown">Ver aplicação
                  <i class="fas fa-angle-right float-right"></i>
                </a>
              </div>

            </div>
            <!--/Small news-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-6 col-md-12">

            <!--Image-->
            <div class="view overlay rounded z-depth-1-half mb-3">
              <img src="assets/img_site/img3.jpg" class="img-fluid" alt="Sample post image">
              <a>
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>

            <!--Excerpt-->
            <h3>
              <a>
                <strong>Componentes</strong>
              </a>
            </h3>

            <!--/Featured news-->

            <hr>

            <!--Small news-->
            <div class="row">
              <div class="col-md-3">

                <!--Image-->
                <div class="view overlay rounded z-depth-1">
                  <img src="assets/img_site/img5.jpg" class="img-fluid" alt="Minor sample post image">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
              </div>

              <!--Excerpt-->
              <div class="col-md-9">
                <p class="dark-grey-text">
                  <strong>Filter Table - Explicação</strong>
                </p>
                <a href="<?php echo base_url(); ?>componentes/filtertable">Ver aplicação
                  <i class="fas fa-angle-right float-right"></i>
                </a>
              </div>

            </div>
            <!--/Small news-->

            <hr>

            <!--Small news-->
            <div class="row">
              <div class="col-md-3">

                <!--Image-->
                <div class="view overlay rounded z-depth-1">
                  <img src="assets/img_site/img6.jpg" class="img-fluid" alt="Minor sample post image">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
              </div>

              <!--Excerpt-->
              <div class="col-md-9">
                <p class="dark-grey-text">
                  <strong>Collapse Duplo</strong>
                </p>
                <a href="<?php echo base_url(); ?>componentes/collapse">Ver aplicação
                  <i class="fas fa-angle-right float-right"></i>
                </a>
              </div>

            </div>
            <!--/Small news-->

            <hr>

            <!--Small news-->
            <div class="row">
              <div class="col-md-3">

                <!--Image-->
                <div class="view overlay rounded z-depth-1">
                  <img src="assets/img_site/img7.jpg" class="img-fluid" alt="Minor sample post image">
                  <a>
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
              </div>

              <!--Excerpt-->
              <div class="col-md-9">
                <p class="dark-grey-text">
                  <strong>Dropdown Search</strong>
                </p>
                <a href="<?php echo base_url(); ?>componentes/dropdown">Ver aplicação
                  <i class="fas fa-angle-right float-right"></i>
                </a>
              </div>

            </div>
            <!--/Small news-->

          </div>
          <!--Grid column-->

        </div>
        <!--Grid row-->

      </section>
      <!--/Section: Magazine v.1-->


    </div>
  </main>
  <!--Main layout-->
