<div style="margin-top:100px; margin-right:50px; margin-left:50px; margin-bottom: 500px;">
<br/>
    <h3 class = "text-center">Utilização de Dropdown</h3>
    <hr/>
        <h4>Visão geral</h4>
            <p style="width:70%">O Dropdown é uma lista de sobreposições contextuais alternáveis para exibir links e outros
                conteúdos. Os dropdowns interagem com o plugin do JavaScript.
                
                No caso de sistemas de navegação mais complexos, o dropdown torna-se indispensável para possibilitar que os
                principais links e demais conteúdos estejam na barra de navegação. Além disso, o dropdown pode ser aplicado
                nos mais diversos contextos. Portanto, trata-se de uma ferramenta muito funcional e atrativa.</p>

    <hr/>
        <h4>Modelo Padrão - HTML</h4>
            <p style="width:70%">Qual single .btn pode ser transformado em um uma lista suspensa com algumas 
                alterações de marcação. Veja abaixo como você pode trabalhar com esses elementos.</p>
            <p style="width:70%">Estilo padrão de menu dropdown.</p>
    
    
    <?= $drop_menu ?>
    <br/>
    <div class="row border rounded-sm grey lighten-3 px-3 mb-0 line-numbers">
                    <pre>
                            <code id="code">
                                    &lt;!-- Basic dropdown --&gt;
                                    &lt;a class="btn btn-primary dropdown-toggle mr-4" type="button" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">Basic dropdown&lt;/a&gt;

                                    &lt;div class="dropdown-menu"&gt;
                                        &lt;a class="dropdown-item" href="#"&gt;Action&lt;/a&gt;
                                        &lt;a class="dropdown-item" href="#"&gt;Another action&lt;/a&gt;
                                        &lt;a class="dropdown-item" href="#"&gt;Something else here&lt;/a&gt;
                                        &lt;div class="dropdown-divider"&gt;&lt;/div&gt;
                                        &lt;a class="dropdown-item" href="#">Separated link&lt;/a&gt;
                                    &lt;/div>
                                    &lt;!-- Basic dropdown --&gt;
                            </code>
                        </pre>
    </div><br/><br/>
    <hr/>
    <h4>Modelo de dropdown com filtro - HTML</h4>
        <p style="width:70%">Modelo de dropdown com o estilo do W3Schools e JavaScript.</p>

    <!-- Dropdown com filtro -->
    <?= $drop_filter_menu ?>
    <br/>
    <div class="row border rounded-sm grey lighten-3 px-3 mb-0 line-numbers">
                    <pre>
                            <code id="code">
                            &lt;div class="dropdown"&gt;
                            &lt;button onclick="myFunction()" class="dropbtn">Dropdown&lt;/button&gt;
                            &lt;div id="myDropdown" class="dropdown-content"&gt;
                                &lt;input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()"&gt;
                                &lt;a href="#about">About&lt;/a&gt;
                                &lt;a href="#base">Base&lt;/a&gt;
                                &lt;a href="#blog">Blog&lt;/a&gt;
                                &lt;a href="#contact">Contact&lt;/a&gt;
                                &lt;a href="#custom">Custom&lt;/a&gt;
                                &lt;a href="#support">Support&lt;/a&gt;
                                &lt;a href="#tools">Tools&lt;/a&gt;
                            &lt;/div&gt;
                            &lt;/div&gt;
                            </code>
                        </pre>
    </div><br/><br/>
    <hr/>
    <h4>Modelo de dropdown com filtro - CSS</h4>
    <div class="row border rounded-sm grey lighten-3 px-3 mb-0 line-numbers">
                    <pre>
                            <code id="code">
                            /* Dropdown Button */
                            .dropbtn {
                            background-color: #4CAF50;
                            color: white;
                            padding: 16px;
                            font-size: 16px;
                            border: none;
                            cursor: pointer;
                            }

                            /* Dropdown button on hover & focus */
                            .dropbtn:hover, .dropbtn:focus {
                            background-color: #3e8e41;
                            }

                            /* The search field */
                            #myInput {
                            box-sizing: border-box;
                            background-image: url('searchicon.png');
                            background-position: 14px 12px;
                            background-repeat: no-repeat;
                            font-size: 16px;
                            padding: 14px 20px 12px 45px;
                            border: none;
                            border-bottom: 1px solid #ddd;
                            }

                            /* The search field when it gets focus/clicked on */
                            #myInput:focus {outline: 3px solid #ddd;}

                            /* The container &lt;div&gt; - needed to position the dropdown content */
                            .dropdown {
                            position: relative;
                            display: inline-block;
                            }

                            /* Dropdown Content (Hidden by Default) */
                            .dropdown-content {
                            display: none;
                            position: absolute;
                            background-color: #f6f6f6;
                            min-width: 230px;
                            border: 1px solid #ddd;
                            z-index: 1;
                            }

                            /* Links inside the dropdown */
                            .dropdown-content a {
                            color: black;
                            padding: 12px 16px;
                            text-decoration: none;
                            display: block;
                            }

                            /* Change color of dropdown links on hover */
                            .dropdown-content a:hover {background-color: #f1f1f1}

                            /* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
                            .show {display:block;}
                            </code>
                        </pre>
    </div><br/><br/>
    <hr/>
    <h4>Modelo de dropdown com filtro - JavaScript</h4>
    <div class="row border rounded-sm grey lighten-3 px-3 mb-0 line-numbers">
                    <pre>
                            <code id="code">
                                    /* When the user clicks on the button,
                                    toggle between hiding and showing the dropdown content */
                                    function myFunction() {
                                    document.getElementById("myDropdown").classList.toggle("show");
                                    }

                                    function filterFunction() {
                                    var input, filter, ul, li, a, i;
                                    input = document.getElementById("myInput");
                                    filter = input.value.toUpperCase();
                                    div = document.getElementById("myDropdown");
                                    a = div.getElementsByTagName("a");
                                    for (i = 0; i < a.length; i++) {
                                        txtValue = a[i].textContent || a[i].innerText;
                                        if (txtValue.toUpperCase().indexOf(filter) &gt; -1) {
                                        a[i].style.display = "";
                                        } else {
                                        a[i].style.display = "none";
                                        }
                                    }
                                }
                            </code>
                        </pre>
    </div><br/><br/>
    <hr/>
</div>