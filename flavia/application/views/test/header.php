<html>
<head>
    <link rel="shortcut icon" href="<?= base_url('assets/img_site/logo2.png') ?>" />


    <title>Flavia - Unit test results</title>

    <style type="text/css">
        * { font-family: Arial, sans-serif; font-size: 15pt }
        #results { width: 100% }
        .err, .pas { color: white; font-weight: bold; margin: 2px 0; padding: 5px; vertical-align: top; }
        .err { background-color: #ff3333 }
        .pas { background-color: #00cc66 }
        .detail { padding: 8px 0 8px 20px }
        h1 { font-size: 20pt }
        a:link, a:visited { text-decoration: none; color: white }
        a:active, a:hover { text-decoration: none; color: black; background-color: #9900ff }
    </style>

</head>
<body>

    <center>
        <h1>Toast - CodeIgniter Unit Test Plugin for Flavia</h1><br>
    </center>
    
<ol>
