<body>

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light deep-purple lighten-3 scrolling-navbar">
      <div class="container">

        <!-- Brand -->
        <img src="<?= base_url('assets/img_site/logohow2use.png') ?>" alt="logo How2Use" 
        title="logo How2Use" width="75" height="65" class="img-fluid"/>

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link waves-effect text-white" href="<?php echo base_url(); ?>principal">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Componentes
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-default"
                aria-labelledby="navbarDropdownMenuLink-333">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>componentes/filtertable">Filter Table</a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>componentes/collapse">Collapse</a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>componentes/dropdown">Dropdown - básico</a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>test/test_flavia">Testes</a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>assets/documentos/index.html">Documentação</a>
                </div>
            </li>
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a href="https://www.facebook.com/" class="nav-link waves-effect text-white" target="_blank">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="https://twitter.com/" class="nav-link waves-effect text-white" target="_blank">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="https://www.linkedin.com/in/flaviamblima/" class="nav-link waves-effect text-white"
                target="_blank">
                <i class="fab fa-linkedin mr-2"></i>
              </a>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

  </header>
  <!--Main Navigation-->