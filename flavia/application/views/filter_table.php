<div style="margin-top:100px; margin-right:50px; margin-left:50px; margin-bottom: 500px;">
<br/>
    <br/>
    <h3 class = "text-center">Tabela com filtro - Filter Table</h3>

    <hr/>
        <p style="width:70%" class="text-center">Aprenda a criar uma tabela com filtro com a utilização de javaScript.</p>
    <hr/>

    <?= $tabela_exemplo ?>
    <br/>
    <hr/>
        <h4>1 - Criar Tabela</h4>
        <p style="width:70%">Primeiramente, crie sua tabela em código HTML. A tabela mostrada abaixo
            foi personalizada, mas você pode utilizar o código apresentado abaixo e realizar as alterações desejadas.
        </p>
    <div class="row border rounded-sm grey lighten-2 px-3 mb-0 line-numbers test-center">
                        <pre>
                            <code id="code">
                            &lt;input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">

                            &lt;table id="myTable"&gt;
                                &lt;tr class="header"&gt;
                                    &lt;th style="width:60%;"&gt;Name&lt;/th&gt;
                                    &lt;th style="width:40%;"&gt;Country&lt;/th&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td&gt;Alfreds Futterkiste&lt;/td&gt;
                                    &lt;td&gt;Germany&lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td&gt;Berglunds snabbkop&lt;/td&gt;
                                    &lt;td>&gt;Sweden&lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td&gt;Island Trading&lt;/td&gt;
                                    &lt;td&gt;UK&lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td&gt;Koniglich Essen&lt;/td&gt;
                                    &lt;td&gt;Germany&lt;/td&gt;
                                &lt;/tr&gt;
                            &lt;/table&gt;
                            </code>
                        </pre>
    </div><br/><br/>
    <hr/>
        <h4>2 - CSS: estilize o elemento de entrada e a tabela</h4>
        <p style="width:70%">Novamente, você pode copiar o CSS e fazer suas próprias alterações.</p>
    
    <div class="row border rounded-sm grey lighten-2 px-3 mb-0 line-numbers">
                    <pre>
                            <code id="code">
                                    #myInput {
                                    background-image: url('/css/searchicon.png'); /* Add a search icon to input */
                                    background-position: 10px 12px; /* Position the search icon */
                                    background-repeat: no-repeat; /* Do not repeat the icon image */
                                    width: 100%; /* Full-width */
                                    font-size: 16px; /* Increase font-size */
                                    padding: 12px 20px 12px 40px; /* Add some padding */
                                    border: 1px solid #ddd; /* Add a grey border */
                                    margin-bottom: 12px; /* Add some space below the input */
                                    }

                                    #myTable {
                                    border-collapse: collapse; /* Collapse borders */
                                    width: 100%; /* Full-width */
                                    border: 1px solid #ddd; /* Add a grey border */
                                    font-size: 18px; /* Increase font-size */
                                    }

                                    #myTable th, #myTable td {
                                    text-align: left; /* Left-align text */
                                    padding: 12px; /* Add padding */
                                    }

                                    #myTable tr {
                                    /* Add a bottom border to all table rows */
                                    border-bottom: 1px solid #ddd;
                                    }

                                    #myTable tr.header, #myTable tr:hover {
                                    /* Add a grey background color to the table header and on hover */
                                    background-color: #f1f1f1;
                                    }
                            </code>
                        </pre>
    </div><br/><br/>
    <hr/>
        <h4>3 - JavaScript: utilize o JS para aplicar o campo de pesquisa</h4>
        <p style="width:70%">Novamente, você pode copiar o CSS e fazer suas próprias alterações.</p>
    <div class="row border rounded-sm grey lighten-2 px-3 mb-0 line-numbers"  allign="center">
                    <pre>
                            <code id="code">
                                &lt;script&gt;
                                    function myFunction() {
                                        // Declare variables
                                        var input, filter, table, tr, td, i, txtValue;
                                        input = document.getElementById("myInput");
                                        filter = input.value.toUpperCase();
                                        table = document.getElementById("myTable");
                                        tr = table.getElementsByTagName("tr");

                                        // Loop through all table rows, and hide those who don't match the search query
                                        for (i = 0; i < tr.length; i++) {
                                            td = tr[i].getElementsByTagName("td")[0];
                                            if (td) {
                                            txtValue = td.textContent || td.innerText;
                                            if (txtValue.toUpperCase().indexOf(filter) &gt; -1) {
                                                tr[i].style.display = "";
                                            } else {
                                                tr[i].style.display = "none";
                                            }
                                            }
                                        }
                                    }
                                    &lt;/script&gt;
                            </code>
                        </pre>
    </div><br/>
    <hr/>
    <!-- Modal -->

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-deep-purple" data-toggle="modal" data-target="#exampleModalCenter">
         Dicas                           
    </button>
    
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">

        <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
        <div class="modal-dialog modal-dialog-centered" role="document">


            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Dicas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                Dica 1: Remova toUpperCase () se desejar executar uma pesquisa que 
                diferencia maiúsculas de minúsculas.</P>

                <p>                    
                Dica 2: Altere tr [i] .getElementsByTagName ('td') [0] para [1] se 
                desejar procurar "País" (índice 1) em vez de "Nome" (índice 0).
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
            </div>
            </div>
        </div>
        </div>
</div>