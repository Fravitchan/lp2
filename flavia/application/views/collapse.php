<div style="margin-top:100px; margin-right:50px; margin-left:50px; margin-bottom: 500px;">
<br/>
    <h3 class = "text-center">Utilização de Collapse</h3>
    <hr/>
        <h4>Collapse</h4>
            <p style="width:90%">Alterne a visibilidade do conteúdo em seu projeto com algumas classes e plugins JavaScript.</p>
            <hr/><h4>Como funciona</h4>
            <p style="width:90%">O collapse com plugin JavaScript é utilizado para exibir e esconder conteúdos. Botões são
                utilizados como gatilhos que são mapeados para elementos específicos que você pode alternar. Recolher um 
                elemento animará o valor atual do height para 0. Em relação ao CSS, você não pode usar padding em um elemento 
                .colappse. Entretanto, você pode utilizar a classe como um elemento de quebra automática.</p>

                <hr/><h4>Exemplo</h4>
                <p style="width:70%">Clique nos botões abaixo para exibir e esconder elementos através de mundanças na class.</p>
                <ul>
                    <li> .collapse esconde o conteúdo</li>
                    <li> .collapsing é aplicado durante as transições</li>
                    <li> .collapse.show exibe o conteúdo</li>
                </ul>
                <p style="width:70%">Você pode utilizar um link com o atributo href ou um botão com o atributo data-target.
                   Em ambos os casos, data-toggle="collapse" é necessário. </p>
                 
    <?= $collapse_elem ?>
    <br/>
    <hr/><h4>1 - HTML:</h4>
    <div class="row border rounded-sm grey lighten-3 px-3 mb-0 line-numbers">
                    <pre>
                            <code id="code">
                            &lt;!-- Collapse buttons --&gt;
                            &lt;div&gt;
                            &lt;a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"&gt;
                                Link with href
                            &lt;/a&gt;
                            &lt;button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample"
                                aria-expanded="false" aria-controls="collapseExample"&gt;
                                Button with data-target
                            &lt;/button&gt;
                            &lt;/div&gt;
                            &lt;!-- / Collapse buttons --&gt;

                            &lt;!-- Collapsible element --&gt;
                            &lt;div class="collapse" id="collapseExample"&gt;
                            &lt;div class="mt-3"&gt;
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim
                                keffiyeh helvetica,
                                craft beer labore wes anderson cred nesciunt sapiente ea proident.
                            &lt;/div&gt;
                            &lt;/div&gt;
                            &lt;!-- / Collapsible element --&gt;
                            </code>
                        </pre>
    </div><br/><br/>


    <hr/><h4>Collapse Múltiplo</h4>
            <p style="width:70%">A tag button ou a tag de links (a), pode mostrar e ocultar vários elementos
            referenciando-os com um seletor JQuery em seu atributo href ou data-target. Vários usos de button 
            podem mostrar e ocultar um elemento se houver uma referência em seu atributo data-target, por exemplo. </p>
    <?= $collapse_dup ?>
    <br/>
    <hr/><h4>2 - HTML do segundo exemplo:</h4>
    <div class="row border rounded-sm grey lighten-3 px-3 mb-0 line-numbers">
                    <pre>
                            <code id="code">
                            &lt;!-- Collapse buttons --&gt;
                            &lt;div&gt;
                            &lt;a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false"
                                aria-controls="multiCollapseExample1"&gt;Toggle first element&lt;/a&gt;
                            &lt;button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#multiCollapseExample2"
                                aria-expanded="false" aria-controls="multiCollapseExample2"&gt;Toggle second element&lt;/button&gt;
                            &lt;button class="btn btn-primary" type="button" data-toggle="collapse" data-target=".multi-collapse"
                                aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2"&gt;Toggle both elements&lt;/button&gt;
                            &lt;/div&gt;
                            &lt;!--/ Collapse buttons --&gt;

                            &lt;!-- Collapsible content --&gt;
                            &lt;div class="row"&gt;
                            &lt;div class="col"&gt;
                                &lt;div class="collapse multi-collapse" id="multiCollapseExample1"&gt;
                                &lt;div class="card card-body"&gt;
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil
                                    anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                &lt;/div&gt;
                                &lt;/div&gt;
                            &lt;/div&gt;
                            &lt;div class="col"&gt;
                                &lt;div class="collapse multi-collapse" id="multiCollapseExample2"&gt;
                                &lt;div class="card card-body"&gt;
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil
                                    anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                                &lt;/div&gt;
                                &lt;/div&gt;
                            &lt;/div&gt;
                            &lt;/div&gt;
                            &lt;!--/ Collapsible content --&gt;
                            </code>
                        </pre>
    </div><br/><br/>
</div>

